import os, sys
from http.server import HTTPServer, CGIHTTPRequestHandler
port = 11145

srvraddr = ('', port)
srvrobj = HTTPServer(srvraddr, CGIHTTPRequestHandler)
srvrobj.serve_forever()
