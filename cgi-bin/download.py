#!/usr/bin/python
import sys

import cgi
form = cgi.FieldStorage()

print('Content-disposition: attachment; filename=code.txt');
print('Content-type: text/html\n')

if 'downloadme' in form:
    print(form['downloadme'].value)

else:
    print('Try entering some code first')
