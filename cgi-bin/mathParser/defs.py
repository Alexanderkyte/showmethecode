derivDefinition = """
def derivativeWrapper(f, precision=0.0000001):
    def derivative(x):
        return (f(x+precision) - f(x))/(precision)
    return derivative
"""

integralDefinition = """
def integralWrapper(f):
    def integralofFun(lower, upper):
        # This is the simpson's approximation. 
        n = 1000000
        h = (upper-lower)/n
        twos = list()
        fours = list()
        for number in range(n):
            if number % 2 == 1:
                twos.append(f(lower+(number*h)))
            elif number % 2 == 0:
                fours.append(f(lower+(number*h)))
        sum = f(lower) + f(upper)
        for number in twos:
            sum += 2 * number
        for number in fours:
            sum += 4 * number
        sum = sum * (h/3)
        return sum

    return integralofFun

"""

summationDefinition = """
def summationWrapper(f):
    def summation(start, stop):
        sum = 0
        for i in range((stop+1)-start):
            sum += f(i+start) # The range(x) form counts from 0 to x-1, so we add one to this. 
        return sum
    return summation
"""

probabilityDefinition = """
def probabilityWrapper(mean, stddev):
    def zscore(value):
        return abs((value - mean)/stddev)
    return zscore
"""
