from mathParser.defs import *

derivativeDefNeeded = False
integralDefNeeded = False

class EmptyNode:
    __slots__ = ()
    def __str__(self):
        return None

class ParseNode:
    #lst should really be "data", as its the data pointer in the linked list, but I like lst, since most of the time its a list
    __slots__ = ('lst', "next")
    def __init__(self, lst):
        self.lst = lst
        self.next = EmptyNode()

    def __str__(self):
        return str(self.lst)

class ArithmeticNode:
    __slots__ = ("op", "args")
    def __init__(self, lst):
        self.op = lst[0]
        self.args = lst[1:]
        
    def eval(self):
        returnList = self.args[0] + self.op
        for term in self.args[1:-1]:
                returnList += term + self.op
        returnList += self.args[-1]
        return returnList

class LiteralNode:
    __slots__ = ('val')
    def __init__(self, lst):
        tmplst = ""
        for i in lst[:-1]:
                tmplst += i + " "
        self.val = tmplst + lst[-1]

    def __str__(self):
        return str(self.val)

    def eval(self):
        return str(self.val)

class SummationNode:
    __slots__ = ("lower", "upper", "function")
    def __init__(self, lst):
        self.function = lst[1]

    def eval(self):
        returnStr = summationDefinition + "summation" + self.function + " = summationWrapper(" + self.function  + ")\n" 
        return returnStr

    def __str__(self):
        return self.eval

class DerivativeNode:
    __slots__ = ("function", "point", "returnStr")
    def __init__(self, lst):
        self.function = lst[1]

    def eval(self):
        returnStr = "deriv" + self.function + " = derivativeWrapper(" + self.function + ")\n"
        return returnStr + derivDefinition

    def __str__(self):
        return self.eval

class IntegralNode:
    __slots__ = ("function", "lower", "upper")
    def __init__(self, lst):
        self.function = lst[0]
    
    def eval(self):
        return "integralWrapper(" + self.function + " )\n" + integralDefinition

    def __str__(self):
        return self.eval


class FunctionAppNode:
    def __init__(self, lst):
        self.name = lst[0]
        args = ""
        lst = lst.split()
        for i in lst:
            if args == "":
                args += i
            else:
                args += ", " + i

        fullString = self.name + "( " + args  + " )\n"
    
    def eval(self):
        return self.fullString

    def __str__(self):
        return self.eval

class ProbabilityNode:
    __slots__ = ("mean", "name", "stddev")
    def __init__(self, lst):
        self.mean = lst[2]
        self.stddev = lst[3]
        self.name = lst[1]

    def eval(self):
        return self.name + " = probabilityWrapper(" + self.mean + ", " + self.stddev + ")\n" + probabilityDefinition 

class FunctionDefNode:
    __slots__ = ("name", "args", "expression")
    def __init__(self, name, lst):
        i = 0
        args = ""
        while True:
            if "{" in lst[i]:
                i += 1
                while "}" not in lst[i]:
                    args += lst[i]
                    i += 1
                break
            i += 1

        expression = ""
        while True:
            if "{" in lst[i] :
                i += 1
                while "}" not in lst[i]:
                    expression += lst[i]
                    i += 1
                break
            i += 1

        self.name = name
        self.args = args
        self.expression = expression

    def eval(self):
        return "def " + self.name + "(" + self.args + "):\n" + "\treturn " + self.expression + "\n"

    def __str__(self):
        return self.eval


def parenParser(parseList, l_brack="(", r_brack=")"):
    i = 0
    parseList = parseList[1:] # strips opening parenthesis
    while i < len(parseList):
        if parseList[i] == r_brack:
            rootNode = ParseNode(parseList[0:i])
            if i < (len(parseList)-1):
                rootNode.next = parenParser(parseList[i+1:])
            return rootNode
        else:
            i += 1

def dumbParser(parseList):
    return ParseNode(parseList)


class VarAssignNode:
    __slots__ = ("var", "value")
    def __init__(self, lst):
        self.var = lst[1]
        self.value = lst[2]

    def eval(self):
        return self.var + " = " + self.value

    def __str__(self):
        return self.eval

def nodeDecider(node):
    op = node.lst[0]

    if op == "+" or  op == "-" or op == "*" or op == "/":
        return ArithmeticNode(node.lst)

    elif op == "eval":
        return LiteralNode(node.lst[1:])

    elif op == "summation":
        return SummationNode(node.lst)

    elif op == "derivative":
        return DerivativeNode(node.lst)
    
    elif op == "=":
        return VarAssignNode(node.lst)
    
    elif op == "probability":
        return ProbabilityNode(node.lst)
    
    elif op == "function":
        functionstr = ""
        name = node.lst[1]
        for each in node.lst[2:]:
            functionstr += str(each)
        return FunctionDefNode(name, functionstr)

    elif op == "integral":
        return IntegralNode(node.lst[1:])

    else:
        return LiteralNode(node.lst)

def listEater(node):
    while not isinstance(node, EmptyNode):
        node.lst = nodeDecider(node)
        node = node.next

def listEvaller(node):
    returnStr = ""
    while not isinstance(node, EmptyNode):
        returnStr += node.lst.eval() 
        node = node.next
    return returnStr


def evaluate(inputString):
    inputString = inputString.split()
    #node = parenParser(inputString)
    node = dumbParser(inputString) #Since I ended up not wanting to provide a recursive descent parser in my site.
    listEater(node)
    return listEvaller(node)

if __name__ == "__main__":
    print(evaluate(input("Enter algebra to evaluate:\n")))
