#!/usr/bin/python
import sys
sys.path.append(sys.path[0] + '/mathParser')
from mathParser.prefixparser import evaluate

import cgi
form = cgi.FieldStorage()

print('Content-type: text/html\n')
print()

results = ""
expression = ""

if 'expression' in form:
	 expression = evaluate( form['expression'].value ) 

if 'results' in form:
    results = form['results'].value

lines = 3

for each in [expression, results]:
        for character in each:
                if character == "\n":
                        lines += 1

print("""
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="../style/formatting.css" />
<html>
<head>
<title>ShowMEtheCODE</title>
<script type='text/javascript'>
window.onload = function() {
    document.getElementsByClassName('entry')[0].focus()
}
</script>
</head>
<body id="body-results">
<div id="container-results">
<div id="innerresultsdiv">
<H1> Show Me the Code: </H1>
<form method=POST action="../cgi-bin/main.py">
<table>
    <TR>
    <TH><input type=text name="expression" value='Enter your maths:' onfocus="if (this.value == 'Enter your maths:') {this.value = ''; this.style.color = '#000'}" onblur="if (this.value == '') {this.value = 'Enter your maths:'; this.style.color = '#b8b8b8'}"  class="entry" id="mainform" style="width: 700px"><select id="helpermenu" style="width: 100px" >
                                                <option value="unset" selected="selected">Help</option>
                                                <option value=" function *function-name* { *args* } { *expression* }">Function Definition</option>
                                                <option value="derivative *function-name*">Derivative</option>
                                                <option value="integral *function-name*">Integral</option>
                                                <option value="summation *function-name*">Summation</option>
                                                <option value="probability *name for function* *mean* *stddev*">Probability</option>
                                        </select>    
	 <TR>
    <TH align=center>Results:
    <TR>
    <TH><textarea class="results" name="results" cols="40" rows=" """ + str(lines) + """ " class="entry textarea" style="width: 800px">""" + results + "\n" + expression + """</textarea>
    <TR>
    <TD colspan=2 align=center>
</table>
</form>
        <script type='text/javascript'> 
        var selectmenu=document.getElementById('helpermenu')
        selectmenu.onchange=function(){ 
                var chosenoption=this.options[this.selectedIndex] 
                        if (chosenoption.value!="unset"){
                            var theform = document.getElementById("mainform")
                            theform.value = chosenoption.value
                            theform.style.color = '#000'
                        }
        }
        </script>

<form method=POST action="../cgi-bin/download.py" name="hiddenlink">
    <input type="hidden" name='downloadme' value='""", results, """'>
</form>
<a align=left href="../grammar.html">Grammar</a> <a align=left href="javascript:void(0)" onclick="document.hiddenlink.submit();">Download</a>
</div>
</div>
</body></html>""")

